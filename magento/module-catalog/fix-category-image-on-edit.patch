Issue: Category image deletes after initial upload and category save v2.3.3
Issue url: https://github.com/magento/magento2/issues/25099
Reported in: 2.3.3
Fixed in: 2.3.4

diff --git a/Model/Category/Attribute/Backend/Image.php b/Model/Category/Attribute/Backend/Image.php
index 6a035a4..4880214 100644
--- a/Model/Category/Attribute/Backend/Image.php
+++ b/Model/Category/Attribute/Backend/Image.php
@@ -121,11 +121,15 @@ class Image extends \Magento\Eav\Model\Entity\Attribute\Backend\AbstractBackend

         if ($this->fileResidesOutsideCategoryDir($value)) {
             // use relative path for image attribute so we know it's outside of category dir when we fetch it
+            // phpcs:ignore Magento2.Functions.DiscouragedFunction
+            $value[0]['url'] = parse_url($value[0]['url'], PHP_URL_PATH);
             $value[0]['name'] = $value[0]['url'];
         }

         if ($imageName = $this->getUploadedImageName($value)) {
-            $imageName = $this->checkUniqueImageName($imageName);
+            if (!$this->fileResidesOutsideCategoryDir($value)) {
+                $imageName = $this->checkUniqueImageName($imageName);
+            }
             $object->setData($this->additionalData . $attributeName, $value);
             $object->setData($attributeName, $imageName);
         } elseif (!is_string($value)) {
@@ -182,7 +186,7 @@ class Image extends \Magento\Eav\Model\Entity\Attribute\Backend\AbstractBackend
             return false;
         }

-        return strpos($fileUrl, $baseMediaDir) === 0;
+        return strpos($fileUrl, $baseMediaDir) !== false;
     }

     /**
diff --git a/Model/Category/FileInfo.php b/Model/Category/FileInfo.php
index d77f472..76b6a2e 100644
--- a/Model/Category/FileInfo.php
+++ b/Model/Category/FileInfo.php
@@ -10,6 +10,8 @@ use Magento\Framework\File\Mime;
 use Magento\Framework\Filesystem;
 use Magento\Framework\Filesystem\Directory\WriteInterface;
 use Magento\Framework\Filesystem\Directory\ReadInterface;
+use Magento\Framework\Exception\NoSuchEntityException;
+use Magento\Store\Model\StoreManagerInterface;

 /**
  * Class FileInfo
@@ -49,15 +51,25 @@ class FileInfo
     private $pubDirectory;

     /**
+     * Store manager
+     *
+     * @var \Magento\Store\Model\StoreManagerInterface
+     */
+    private $storeManager;
+
+    /**
      * @param Filesystem $filesystem
      * @param Mime $mime
+     * @param StoreManagerInterface $storeManager
      */
     public function __construct(
         Filesystem $filesystem,
-        Mime $mime
+        Mime $mime,
+        StoreManagerInterface $storeManager
     ) {
         $this->filesystem = $filesystem;
         $this->mime = $mime;
+        $this->storeManager = $storeManager;
     }

     /**
@@ -152,7 +164,8 @@ class FileInfo
      */
     private function getFilePath($fileName)
     {
-        $filePath = ltrim($fileName, '/');
+        $filePath = $this->removeStorePath($fileName);
+        $filePath = ltrim($filePath, '/');

         $mediaDirectoryRelativeSubpath = $this->getMediaDirectoryPathRelativeToBaseDirectoryPath($filePath);
         $isFileNameBeginsWithMediaDirectoryPath = $this->isBeginsWithMediaDirectoryPath($fileName);
@@ -177,7 +190,8 @@ class FileInfo
      */
     public function isBeginsWithMediaDirectoryPath($fileName)
     {
-        $filePath = ltrim($fileName, '/');
+        $filePath = $this->removeStorePath($fileName);
+        $filePath = ltrim($filePath, '/');

         $mediaDirectoryRelativeSubpath = $this->getMediaDirectoryPathRelativeToBaseDirectoryPath($filePath);
         $isFileNameBeginsWithMediaDirectoryPath = strpos($filePath, (string) $mediaDirectoryRelativeSubpath) === 0;
@@ -186,6 +200,30 @@ class FileInfo
     }

     /**
+     * Clean store path in case if it's exists
+     *
+     * @param string $path
+     * @return string
+     */
+    private function removeStorePath(string $path): string
+    {
+        $result = $path;
+        try {
+            $storeUrl = $this->storeManager->getStore()->getBaseUrl();
+        } catch (NoSuchEntityException $e) {
+            return $result;
+        }
+        // phpcs:ignore Magento2.Functions.DiscouragedFunction
+        $path = parse_url($path, PHP_URL_PATH);
+        // phpcs:ignore Magento2.Functions.DiscouragedFunction
+        $storePath = parse_url($storeUrl, PHP_URL_PATH);
+        $storePath = rtrim($storePath, '/');
+
+        $result = preg_replace('/^' . preg_quote($storePath, '/') . '/', '', $path);
+        return $result;
+    }
+
+    /**
      * Get media directory subpath relative to base directory path
      *
      * @param string $filePath
