# magento2-patches
Patches that relate to specific PR's to be used with https://github.com/cweagans/composer-patches

Original comes from https://github.com/allanpaiste/magento2-patches. Thanks Allan!

## Usage

The patches have been created to be used for composer M2 projects.

So if you have used steps described at http://devdocs.magento.com/guides/v2.0/install-gde/prereq/integrator_install.html, 
you should have a M2 project that has composer.json at it's core that dicrates which packages are installed.

1. In project root folder: composer require cweagans/composer-patches=~1.4.0
2. Copy the patches you want to use under <project-root>/patches
3. The project composer.json should also already have an "extra" key defined. if you add new sub-key there called 
   "patches" (see above), then the next time you do "composer update", the modules that have patches defined will 
   be re-installed and patches applied. Patches have file-target paths that are relative to composer packages which 
   means that the context defined in composer.json is pretty important and patches are not directly appliable in context 
   on magento/magento2 repository, but they're ripe to be used for composer project.
   
```json
{

    "require": {
        "cweagans/composer-patches": "~1.4.0",
    },

    "extra": {
        "patches": {
            "magento/module-braintree": {
                "Fix: https://github.com/magento/magento2/issues/12298":
                "https://bitbucket.org/redkiwi/magento2-patches/raw/41da7af2bbda123ecdaf6001750d5700c6376d18/Patch-Magento22x-Braintree-undefined-array.patch"
            }
        }
    }

}
```